function [k] = rate_const(T, D0, Td, z)
% Calculate the rate constant given the temperature, the D and z values.
%   Additionally the temperature at which D was taken is supplied.
%   T - Temperature (K)
%   D0 - Time to achieve a decimal reduction in concentration (min)
%   Td - Temperature at which D is valid.
%   z - z value (F)
%   k - Rate constant (1/s)

% Convert T from Kelvin to Fahrenheit.
T = (T - 273.15)*9/5 + 32;

Dt = D0*10^((Td-T)/z); % Eq 9.12-12
%k = 1/(log10(exp(1))*Dt);
%k = 2.303/Dt;
k = -log(0.1)/Dt; % From Eq 9.12-5

% Convert the rate constant from 1/min to 1/s
k = k * 1/60;

