function [alpha] = alpha_pumpkin(T)
% Calculate the thermal diffusivity of pumpkin pulp as a function of
% temperature using the Choi-Okos equations. Valid between -40C and 150C.
%   T - Temperature (K)
%   alpha - Thermal diffusivity (m^2/s)

% Convert from Kelvin to Celcius
T = T-273.15;

% Mass fraction of each component in pumpkin pulp.
MPro = 0.03070;
MFat = 0.02300;
MCar = 0.66647;
MFib = 0.11463;
MAsh = 0.15988;
MWat = 0.00532;
% Source: Adebayo, et al., 2013, Proximate, Mineral, and Anti-Nutrient
% Evaluation of Pumpkin Pulp (Cucurbita Pepo).

% Thermal diffusivity as a function of temperature. Equations taken from Table
% II and Table IV from Choi and Okos 1986.
aPro = 6.8714e-2 + 4.7578e-4 * T - 1.4646e-6 * T^2;
aFat = 9.8777e-2 - 1.2569e-5 * T - 2.8286e-8 * T^2;
aCar = 8.0842e-2 + 5.3052e-4 * T - 2.3218e-6 * T^2;
aFib = 7.3976e-2 + 5.1902e-4 * T - 2.2202e-6 * T^2;
aAsh = 1.2461e-1 + 3.7321e-4 * T - 1.2244e-6 * T^2;
aWat = 1.3168e-1 + 6.2477e-4 * T - 2.4022e-6 * T^2;

% Density as a function of temperature. Equations from Table II and IV of Choi
% and Okos 1986.
pPro = 1.3299e3 - 5.1840e-1 * T;
pFat = 9.2559e2 - 4.1757e-1 * T;
pCar = 1.5991e3 - 3.1046e-1 * T;
pFib = 1.3115e3 - 3.6589e-1 * T;
pAsh = 2.4238e3 - 2.8063e-1 * T;
pWat = 997.18 + 3.1439e-3 * T - 3.7574e-3* T^2;

% Calculate the volume fraction of each component.
Vtot = (MWat/pWat + MPro/pPro + MFat/pFat + MCar/pCar + MFib/pFib + MAsh/pAsh);
XvPro = (MPro/pPro) / Vtot;
XvFat = (MFat/pFat) / Vtot;
XvCar = (MCar/pCar) / Vtot;
XvFib = (MFib/pFib) / Vtot;
XvAsh = (MAsh/pAsh) / Vtot;
XvWat = (MWat/pWat) / Vtot;

% Calculate the overall thermal diffusivity of pumpkin pulp at the desired
% temperature.
alpha = aPro*XvPro + aFat*XvFat + aCar*XvCar + aFib*XvFib + aAsh*XvAsh + ...
        aWat*XvWat;

% The Choi-Okos equations provide thermal diffusivity in terms of mm^2/s.
% Convert to m^2/s.
alpha = alpha * 1e-6;

end

