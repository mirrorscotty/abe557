function [avg] = average_value(nodes)
% Determine the average value along the radius of the can.
%   nodes - Vector of the values of the variable to be averaged at each of the
%       nodes. The first element corresponds to r=0, and the last corresponds
%       to r=R.
%   avg - Average value

global dx;
global radius;

avg = 0; % Integral of f(r) from r=0 to r=R
denom = 0; % Integral of 1 from r=0 to r=R

% Calculate the integrals numerically.
for i = 1:length(nodes)
    r = (i-1)*dx;
    avg = avg + nodes(i) * r*dx;
    denom = denom + r*dx;
end

% Find the average value
avg = avg/denom;

