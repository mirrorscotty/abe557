% Run this script to generate the solution.

%---- Given Information --------------------------------------------------------
X = 0;

global radius;
radius = 3 + 0.1*X; %in

global Tcool Theat Tfinal Tinitial hconv;
Tinitial = 180; % F
Tcool = 50; % F
Tfinal = 100; % F
% If the convective heat transfer coefficient is set to zero, then the
% outermost node's temperature will simply be set to whatever the ambient
% temperature is. Otherwise, it'll be calculated using Eq. 5.4-23.
hconv = 0; % W/(m^2 K)

global CG0 CV0 CGfinal;
CG0 = 1; % dimensionless - Initial microbe concentration
CV0 = 1; % dimensionless - Initial vitamin concentration
CGfinal = 1e-12; % dimensionless - Maximum allowable microbe concentration

global DG zG DV zV Td;
DG = 0.21; % min
zG = 17.8; % degrees F
DV = 164; % min
zV = 47; % degrees F
Td = 250; % degrees F

%---- Unit Conversions ---------------------------------------------------------
% Convert the radius to meters
radius = radius * 0.0254;
% Convert all the temperatures to Kelvin
Tcool = (Tcool-32)*5/9 + 273.15;
Tfinal = (Tfinal-32)*5/9 + 273.15;
Tinitial = (Tinitial-32)*5/9 + 273.15;

%---- Part A -------------------------------------------------------------------
% Global variables that are set in the sterilize function.
global dx;
global nnodes;
global dt;
global Theat;
global Tcool;

% Create a vector to hold the x (or in this case, r) values of each node.
x(1) = 0;
for i = 2:nnodes
    x(i) = x(i-1) + dx;
end

% Calculate the temperature and concentration profiles as well as the required
% heating and cooling times for a sterilization process at 250 F.
[CV, values, theat, tfinal] = sterilize(250);
tcool = tfinal - theat;

% Plot the temperature profiles across the radius of the can at t=0, the point
% at which the product is sterilized, and two profiles between those times.
figure(1);
plot(x, values(1, :, 1),...
     x, values(floor(theat/3),:,1),...
     x, values(floor(2*theat/3),:,1),...
     x, values(theat,:,1));
% Generate a legend to indicate which times are used.
legend('t=0 min', sprintf('t=%g min', floor(theat/3)*dt/60), sprintf('t=%g min', floor(2*theat/3)*dt/60), sprintf('t=%g min', theat*dt/60));
title('Temperature profiles during heating');
xlabel('Radius (m)');
ylabel('Temperature (K)');

figure(2);
% Do the same for the cooling process
plot(x, values(theat, :, 1),...
     x, values(floor(tcool/3)+theat,:,1),...
     x, values(floor(2*tcool/3)+theat,:,1),...
     x, values(tfinal,:,1));
legend('t=0 min', sprintf('t=%g min', floor(tcool/3+theat)*dt/60), sprintf('t=%g min', floor(2*theat/3+theat)*dt/60), sprintf('t=%g min', tfinal*dt/60));
title('Temperature profiles during cooling');
xlabel('Radius (m)');
ylabel('Temperature (K)');

%---- Part B -------------------------------------------------------------------
% Create vectors of the values of the rate constants for the decomposition of
% both microbes and vitamins. 
T = linspace(Tcool, Theat);
for i = 1:length(T)
    kG(i) = rate_const(T(i), DG, Td, zG);
    kV(i) = rate_const(T(i), DV, Td, zV);
end

% Plot the values
figure(3);
plot(1./T, log(kG),...
     1./T, log(kV));
legend('Microbe death', 'Vitamin decomposition');
title('Rate constants for microbe death and nutrient decomposition');
xlabel('1/T (K)');
ylabel('ln k');

%---- Part C -------------------------------------------------------------------
% Print out the processing times.
fprintf('Heating time: %g min\nCooling time: %g min\nTotal time: %g min\n', theat*dt/60, tcool*dt/60, tfinal*dt/60);

%---- Part D -------------------------------------------------------------------
figure(4);
% Plot the final microbe and nutrient concentrations and print out the
% percentage of the original that remains.
plot(x, values(tfinal, :, 2));
title('Final microbe concentration');
xlabel('Radius (m)');
ylabel('Dimensionless concentration (C/C0)');

figure(5);
plot(x, values(tfinal, :, 3));
title('Final vitamin concentration');
xlabel('Radius (m)');
ylabel('Dimensionless concentration (C/C0)');

fprintf('Remaining amount of microbes: %g%%\nRemaining amount of nutrient: %g%%\n', average_value(values(tfinal,:,2)), average_value(values(tfinal,:,3)));

%---- Part E -------------------------------------------------------------------
% Plot the remaining vitamin concentration verses temperature to find the
% optimal sterilization temperature.
T = linspace(215, 230, 50);
for i = 1:length(T)
    CV(i) = sterilize(T(i));
end

figure(6);
plot(T, CV);
title('Remaining vitamin as a function of sterilization temperature');
xlabel('Sterilization temperature (F))');
ylabel('Final average vitamin concentration (C/C0)');

% Output the optimal temperature. It may be necessary to adjust the temperature
% range and number of points sampled to improve this value. For the current X
% value, the chosen range appears to work well.
[CVopt, index] = max(CV);
fprintf('Optimal sterilization temperature: %g\n', T(index));
% Any "wiggles" in this graph are due to errors associated with the numerical
% methods used. To smooth it out, increase the M value in the finite difference
% function, or increase the number of nodes used (or both).

