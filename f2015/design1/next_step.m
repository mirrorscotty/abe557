function [output] = next_step(input, t, Tboundary)
% Calculate the temperature and concentrations at each node given the values at
% the previous time step and the temperature at the boundary.
%   in - Matrix of temperatures and concentrations at the previous time
%       step. Temperatures are given in Kelvin, and concentrations are
%       dimensionless.
%   Tboundary - Ambient temperature outside of the can. (Kelvin)
%   out - Matrix of the same form as the input matrix. This contains the
%       values at the next time step.

global Tcol;
global Gcol;
global Vcol;
global dx;
global dt;
global nnodes;
global DG;
global zG;
global DV;
global zV;
global Td;
global hconv;

output = input;
R = 8.314;
T = input(t, :, Tcol);
CG = input(t, :, Gcol);
CV = input(t, :, Vcol);

% Equation 5.4-3
M = dx^2/(alpha_pumpkin(T(1)) * dt);
% Equation 5.4-21
output(t+1, 1, Tcol) = 4/M * T(2) + (M-4)/M*T(1);
for n = 2:nnodes-1
    M = dx^2/(alpha_pumpkin(T(n)) * dt);
    % Equation 5.4-20
    output(t+1, n, Tcol) = 1/M * ((2*n+1)/(2*n)*T(n+1) + (M-2)*T(n) + (2*n-1)/(2*n)*T(n-1));
end
if hconv > 0
    % Equation 5.4-8
    N = hconv * dx / k_pumpkin(T(n));
    % Equation 5.4-23
    output(t+1, nnodes, Tcol) = n*N/((2*n-1)/2 + n*N) * Tboundary + (2*n-1)/2/((2*n-1)/2+n*N)*T(n);
else
    output(t+1, nnodes, Tcol) = Tboundary;
end

for n = 1:nnodes
    Ctmp = CG(n) + dt * ( -CG(n)*rate_const(T(n), DG, Td, zG) );
    if Ctmp < 0
        Ctmp = 0;
    end
    output(t+1, n, Gcol) = Ctmp;
    Ctmp = CV(n) + dt * ( -CV(n)*rate_const(T(n), DV, Td, zV) );
    if Ctmp < 0
        Ctmp = 0;
    end
    output(t+1, n, Vcol) = Ctmp;
end

