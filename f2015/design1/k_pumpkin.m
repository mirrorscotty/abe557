function [k] = k_pumpkin(T)
% Calculate the thermal conductivity of pumpkin pulp as a function of
% temperature using the Choi-Okos equations. Valid between -40C and 150C.
%   T - Temperature (K)
%   k - Thermal conductivity (W/m-K)

% Convert from Kelvin to Celcius
T = T-273.15;

% Mass fraction of each component in pumpkin pulp.
MPro = 0.03070;
MFat = 0.02300;
MCar = 0.66647;
MFib = 0.11463;
MAsh = 0.15988;
MWat = 0.00532;
% Source: Adebayo, et al., 2013, Proximate, Mineral, and Anti-Nutrient
% Evaluation of Pumpkin Pulp (Cucurbita Pepo).

% Thermal diffusivity as a function of temperature. Equations taken from Table
% II and Table IV from Choi and Okos 1986.
kPro = 1.7881e-1 + 1.1958e-3 * T - 2.7178e-6 * T^2;
kFat = 1.8071e-1 - 2.7604e-4 * T - 1.7749e-7 * T^2;
kCar = 2.0141e-1 + 1.3874e-3 * T - 4.3312e-6 * T^2;
kFib = 1.8331e-1 + 1.2497e-3 * T - 3.1683e-6 * T^2;
kAsh = 3.2962e-1 + 1.4011e-3 * T - 2.9069e-6 * T^2;
kWat = 5.7109e-1 + 1.762e-3 * T - 6.703e-6 * T^2;

% Density as a function of temperature. Equations from Table II and IV of Choi
% and Okos 1986.
pPro = 1.3299e3 - 5.1840e-1 * T;
pFat = 9.2559e2 - 4.1757e-1 * T;
pCar = 1.5991e3 - 3.1046e-1 * T;
pFib = 1.3115e3 - 3.6589e-1 * T;
pAsh = 2.4238e3 - 2.8063e-1 * T;
pWat = 997.18 + 3.1439e-3 * T - 3.7574e-3* T^2;

% Calculate the volume fraction of each component.
Vtot = (MWat/pWat + MPro/pPro + MFat/pFat + MCar/pCar + MFib/pFib + MAsh/pAsh);
XvPro = (MPro/pPro) / Vtot;
XvFat = (MFat/pFat) / Vtot;
XvCar = (MCar/pCar) / Vtot;
XvFib = (MFib/pFib) / Vtot;
XvAsh = (MAsh/pAsh) / Vtot;
XvWat = (MWat/pWat) / Vtot;

% Calculate the overall thermal diffusivity of pumpkin pulp at the desired
% temperature.
k = kPro*XvPro + kFat*XvFat + kCar*XvCar + kFib*XvFib + kAsh*XvAsh + ...
        kWat*XvWat;

end

