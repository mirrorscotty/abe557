function [CV, values, theat, tfinal] = sterilize(T)
% Simulate sterilization of a cylindrical can.
%   T - Maximum sterilization temperature (F)
%   CV - Average vitamin concentration after sterilization and cooling
%       (dimensionless)
%   values - Temperatures and concentrations at each node at each time step.
%       The first index is the time step number, the second is the node number,
%       and the third is the variable number. Temperatures are in
%       Kelvin and concentrations are dimensionless.
%   theat - Required heating time to achieve sterilization (s)
%   tcool - Required time to cool the can down to an average temperature of
%       100F (s)

%---- Given information --------------------------------------------------------
global radius hconv;
global Tcool Theat Tfinal Tinitial;
global CG0 CV0 CGfinal;
global DG zG DV zV Td;

Theat = T;

%---- Unit Conversions ---------------------------------------------------------
% Convert from Fahrenheit to Kelvin
Theat = (Theat-32)*5/9 + 273.15;

%---- Finite difference parameters ---------------------------------------------
% Number of nodes to use when solving. Higher values yield more accurate
% answers, but longer computation times.
global nnodes;
nnodes = 11; 
% Minimum value for the M parameter. This is used to calculate the time step
% size. A value of four or larger guarantees convergence for this problem;
% however, the accuracy (and computation time) increase with larger values.
% Because the reaction rates occur at a different time scale than heat transfer
% does, this value is large so that the time step size is smaller.
Mmin = 20;

% Calculate dx
global dx;
dx = radius / (nnodes - 1);

% Find dt
% First make a matrix of all the values that thermal diffusivity could have
for i = 1:100
    T(i) = Tcool + (Theat-Tcool)/100 * i;
    alpha(i) = alpha_pumpkin(T(i));
    k(i) = k_pumpkin(T(i));
end
% Then choose the largest one and use that to calculate the dt value
global dt;
dt = dx^2 / (max(alpha) * Mmin); % From Eq. 5.4-3

if hconv > 0
    % Check that the M is large enough for the largest possible value of N.
    Nmax = hconv * dx / min(k);
    if Mmin < 2*Nmax + 2 % Equation 5.4-9
        fprintf('The selected value of M is too small for the supplied heat transfer coefficient.\n');
    end
end

% Matlab automatically resizes arrays when you try to add too much to them, so
% just set the number of time steps to one.
nsteps = 1;

% Set up a matrix to hold everything. The first index is the time step, the
% second is the node number, and the third is the variable we're looking at.
global Tcol;
Tcol = 1; % Store temperature data in the first column
global Gcol;
Gcol = 2; % Germ concentration goes in the second
global Vcol;
Vcol = 3; % Vitamin concentration goes in the third

% Fill the matrix with zeros initially.
values = zeros([nsteps, nnodes, 3]);

%---- Set the initial conditions -----------------------------------------------
for i = 1:nnodes
    values(1, i, Tcol) = Tinitial;
    values(1, i, Gcol) = CG0;
    values(1, i, Vcol) = CV0;
end

%---- Find sterilization time --------------------------------------------------
t = 1; % Current time step number. A value of 1 indicates the first time step.

% Loop as long as the germ concentration in the center of the can is greater
% than the minimum required concentration for sterilization.
while values(t, 1, Gcol) > CGfinal
%while t < 5
    % Calculate the temperature and concentrations at the next time step
    values = next_step(values, t, Theat);
    % Increment the time step number
    t = t+1;
end

% After we're done looping, we'll know the time required to sterilize the can.
theat = t;

%---- Find cooling time --------------------------------------------------------
% Figure out how long it takes to cool the can to the desired temperature. This
% time, we're looking at the average temperature rather than just the value at
% the center of the can.
while average_value(values(t, :, Tcol)) > Tfinal
    % Calculate the temperature and concentrations at the next time step
    values = next_step(values, t, Tcool);
    % Increment the time step number
    t = t+1;
end

% Once the loop is done executing, we'll know how long it takes to cool the can
% to the final temperature.
tfinal = t;

% Find the average value of vitamin concentration and return it.
CV = average_value(values(t, :, Vcol));

end

