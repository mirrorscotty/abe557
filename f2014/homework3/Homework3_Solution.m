%Part A:
clear all
%Given Data in table:
TempC = [5 15 25 35 45 55];
Temp = TempC+273; % Convert from Celcius to Kelvin
Xm = [5.632,5.4031,4.94,4.6299,4.4561,4.0165];
K = [0.8923,0.8936,0.9298,0.9410,0.9359,0.9542];
Cg = [89.5404,40.6913,48.7413,29.569,17.009,13.3139];
% Linearized equations by using natural log
lnXm = log(Xm);
lnK = log(K);
lnCg = log(Cg);
lnT = 1./Temp;
% Find the fitted models for the given data,
[Xmf] = polyfit(lnT,lnXm,1);
[Kf] = polyfit(lnT,lnK,1);
[Cgf] = polyfit(lnT,lnCg,1);
%Use the slope and intercept:
Xm0 = exp(Xmf(2));
dHxm = Xmf(1); 
K0 = exp(Kf(2)); 
dHk = Kf(1); 
Cg0 = exp(Cgf(2));
dHcg = Cgf(1);
% And then the needed plots of each function can be done by preference, 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Moisture (x) vs water activity (aw)
aw =[0:0.01:0.89]; %water activity range
for t = [5:10:55]
    T = t + 273; 
    % GAB equation
    X(1,:) = ((Xm0*exp(dHxm/T))*(K0*exp(dHk/T))*(Cg0*exp(dHcg/T)).*aw)./(((1-(K0*exp(dHk/T)).*aw)).*(1+((Cg0*exp(dHcg/T))-1).*aw.*(K0*exp(dHk/T))));
    % Plot of M vs aw
    figure(1)
    plot(aw,X(1,:));
    hold on;
end
legend('Temp 5 C','15 C','25 C','35 C','45 C','55 C')
xlabel('Water Activity')
ylabel('Moisture Content [g/kg db]')
hold off;

% Part B:
clear; % Get rid of the values from part A
X = linspace(0, 50); % Generate a bunch of moisture contents.
T = 5:10:55; % Select the temperatures we'll be plotting at.
T = T+273; % Convert from Celcius to Kelvin

% Calculate binding energy at each moisture content and temperature
for i=1:length(X)
    for j = 1:length(T)
        Eb(i,j) = binding_energy(X(i),T(j));
    end
end

% Plot all of the binding energies on figure 2.
for j = 1:length(T)
    figure(2)
    plot(X, Eb(:,j));
    hold on
end
legend('Temp 5 C','15 C','25 C','35 C','45 C','55 C')
xlabel('Moisture Content [g/kg db]');
ylabel('Binding Energy [J/mol]');

% Part C:
clear; % Clear all the variables
X = linspace(0, 50); % Make a bunch of moisture contents.
T = 5:10:55; % Select the temperatures for the plot.
T = T+273; % Convert from C to K
phi = 0:.1:.5; % Porosities to plot diffusivity at.

% Calculate effective diffusivity at all the moisture contents and
% temperatures, but at 0% porosity.
for i=1:length(X)
    for j = 1:length(T)
        Deff(i,j) = diffusivity(X(i),T(j),phi(1));
    end
end

% Plot Deff vs T on figure 3.
for j = 1:length(T)
    figure(3)
    plot(X, Deff(:,j));
    hold on
end
legend('Temp 5 C','15 C','25 C','35 C','45 C','55 C')
xlabel('Moisture Content [g/kg db]');
ylabel('Diffusivity [m^2/s]');
title('Diffusivity vs Moisture Content at 0% Porosity');

% Calculate diffusivity at all moisture contents and porosities, but at
% 5 C.
for i=1:length(X)
    for j=1:length(phi)
        Deff(i,j) = diffusivity(X(i),T(1),phi(j));
    end
end

% Plot the results in figure 4.
for j = 1:length(phi)
    figure(4)
    plot(X, Deff(:,j));
    hold on
end
legend('Porosity 0%','10%','20%','30%','40%','50%')
xlabel('Moisture Content [g/kg db]');
ylabel('Diffusivity [m^2/s]');
title('Diffusivity vs Moisture Content at T=5 C');
