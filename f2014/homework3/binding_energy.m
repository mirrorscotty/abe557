function [ Eb ] = binding_energy( X, T )
% Calculate the binding energy based on the GAB equation.
%       X: Moisture content [g/kg db]
%       T: Temperature [K]
%   This function calculates binding energy using the Clausius-Clayperon
%   equation. The calculated value has units of J/mol.

h = 1e-5; % dx value to use for numeric differentiation.
R = 8.314; % Gas constant [J/(mol K)]

% Calculate the derivative of ln(aw) with respect to temperature
% using the central difference formula.
DlnawDT = (log(gab_aw(X, T+h))-log(gab_aw(X, T-h)))/(2*h);

% Calculate binding energy [J/mol]
Eb = T*T*R*DlnawDT;

end

