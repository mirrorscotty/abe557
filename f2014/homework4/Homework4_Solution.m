clear;
% -------------------------------------------------------------------------
% --- Part A --------------------------------------------------------------
% -------------------------------------------------------------------------
% Plot the glass transition temperature, as well as Tg+10C and Tg+40C.
TLow = 10;
THigh = 40;
Xwb = linspace(0, .35);
for i=1:length(Xwb)
    Tg(i)=gordontaylor(Xwb(i)); % Glass transition temperature
    Tg10(i)=Tg(i)+TLow; % Glass transition temperature + 10 C
    Tg40(i)=Tg(i)+THigh; % Glass transition temperature + 40 C
end
% Plot all three curves on one graph
figure(1);
plot(Xwb,Tg);
hold on;
plot(Xwb,Tg10);
plot(Xwb,Tg40);
hold off;
title('Glass Transition Temperature (and Tg+10 C, Tg+40 C)');
xlabel('Moisture Content [kg/kg wb]');
ylabel('Glass Transition Temperature [K]');

% Get rid of useless variables.
clearvars Xwb Tg Tg40 Tg10

% Step off stages of the dryer
% Input: Xwb = .3
Xinit = .3; % Initial (wet basis) moisture content
% Stage 1:
Tst(1) = gordontaylor(Xinit)+THigh; % Find the temperature of the first stage
% Make a function to solve in order to find the moisture content that
% corresponds to Tg(Xwb) + 10 = Tst(1).
f = @(x)(gordontaylor(x)+TLow-Tst(1));
% Solve for Xwb. This is the final moisture content for the stage.
Xst(1) = fsolve(f, Xinit, optimset('Display','off'));

% Do the same for the rest of the stages. Here, i represents the stage
% number.
i=2; % Start at stage 2
% Keep going until the end moisture content is less than .05
while(Xst(i-1) > .05)
    % This is exactly the same as above
    Tst(i) = gordontaylor(Xst(i-1))+THigh;
    f = @(x)(gordontaylor(x)+TLow-Tst(i));
    Xst(i) = fsolve(f, Xst(i-1), optimset('Display','off'));
    i=i+1; % Set i equal to the number for the next stage
end

% Figure out how many stages we calculated moisture contents for.
Nst = length(Xst);

% Calculate the equilibrium moisture content at the first stage
Xeq(1) = Xinit-(Xinit-Xst(1))/.9;
% Then do the same for the rest of the stages
for i=2:Nst
    Xeq(i) = Xst(i-1)-(Xst(i-1)-Xst(i))/.9;
end
    
% Convert to dry basis. We'll be using dry basis moisture content from now
% on because it's what the GAB uses to calculate water activity.
Xeqd = Xeq./(1-Xeq);
Xstd = Xst./(1-Xst);

% Find the relative humidity of each stage based on the water activity
% calculated from the GAB equation.
for i=1:Nst
    RH(i) = gab_aw(Xeqd(i), Tst(i));
end

% Print out the temperature and relative humidity for each stage.
fprintf('Stage conditions:\n');
for i=1:Nst
    fprintf('Stage %d: Temperature %g K; RH: %g; Xeq: %g; Xst: %g\n', i, Tst(i), RH(i), Xeqd(i), Xstd(i));
end
% Print out incorrect conditions based on wet-basis moisture content.
fprintf('Stage conditions:\n');
for i=1:Nst
    fprintf('Stage %d: Temperature %g K; RH: %g; Xeq: %g; Xst: %g\n', i, Tst(i), gab_aw(Xeq(i), Tst(i)), Xeq(i), Xst(i));
end

% -------------------------------------------------------------------------
% --- Part B --------------------------------------------------------------
% -------------------------------------------------------------------------
% Slab half thickness (initial)
L = 0.01/2; % m
% Initial concentration (kg/kg db)
c0 = Xinit/(1-Xinit); % Convert from wet basis
% Delta X value to use when solving (initial)
dxi = 0.001; % m
% Porosity
phi = 0.0;

% Convective mass transfer coefficient
% First determine an approximation for the average diffusivity.
Davg = .5*(diffusivity(c0, Tst(1), phi) + diffusivity(Xst(Nst), Tst(Nst), phi));
Bi = 10; % = kc*L/D (Biot number for mass transfer)
kc = Bi*Davg/L;

% Find a representative N value we'll use to determine our dt
N = kc*dxi/Davg;
% Do the same for M
M = 2*N+2;
% Make the M value larger just to make sure we don't run into convergence
% problems later. The amount we add here might need to be adjusted later
% on, depending on how the solution converges.
M = M + 5;

% Calculate the time step size based on the M value we just calculated.
% This value will not change as we're solving the problem.
dt = dxi^2/(Davg*M); % M = (dx)^2/(Dab*dt)

% Calculate the number of nodes to use.
nx = L/dxi;

%--- Impose Initial Condition ---------------------------------------------
% Set the initial condition. Here, the variable c will hold all of the
% values we calculate. The i index will represent the current time step,
% and the j index will be the current point along the x-axis.
for j = 1:nx
    c(1,j) = c0;
    dx(1,j) = dxi;
end

%--- Finite Difference Calculations ---------------------------------------
i=2; % Start the time at time step #2 because #1 is the initial condition
Xavg = sum(c(i-1,:))/nx; % Find the average moisture content initially.
% Since we have several stages, each with different boundary conditions, do
% a for loop to set the boundary conditions at each stage.
for k = 1:Nst
    % Calculate the concentration profiles at each time step. This loop
    % will run until the average moisture content of the product is less
    % than or equal to the final concentration for that stage that we
    % calculated earlier.
    while(Xavg(i-1)>Xstd(k))
        % Impose the convective boundary condition at x=0. Both M and N are
        % recalculated here as a function of the dx value.
        M = dx(i-1, 1)^2/(diffusivity(c(i-1, 1), Tst(k), phi)*dt);
        N = kc*dx(i-1,1)/diffusivity(c(i-1, 1), Tst(k), phi);
        c(i,1) = 1/M*(2*N*Xeqd(k)+(M-(2*N+2))*c(i-1,1)+2*c(i-1,2));
        
        % Calculate the concentration at each point along the interior of
        % the slab. Again, M is recalculated using the dx value for the
        % current node.
        for j = 2:nx-1
            M = dx(i-1, 1)^2/(diffusivity(c(i, 1), Tst(k), phi)*dt);
            c(i,j) = 1/M * (c(i-1,j+1) + (M-2)*c(i-1,j) + c(i-1,j-1));
        end
        
        % At x=L, impose an insulated boundary condition.
        M = dx(i-1, 1)^2/(diffusivity(c(i, 1), Tst(k), phi)*dt);
        c(i,nx) = 1/M * ((M-2)*c(i-1,nx) + 2*c(i-1,nx-1));
        
        % Recalculate dx for each node based on the change in density of
        % the product due to loss of moisture. This assumes the porosity is
        % always equal to zero.
        for j=1:nx
            dx(i,j) = dx(i-1,j)*rhosoy(c(i-1,j),Tst(k))/rhosoy(c(i,j),Tst(k));
        end
        
        % Find the average moisture content of the slab at the current time
        % step.
        Xavg(i) = sum(c(i,:))/nx;
        % If the average moisture content is less than the final moisture
        % we're drying our product to (here .05 wb converted to dry basis),
        % then we should stop drying things.
        if(Xavg(i)<(.05/(1-.05)))
            break;
        end
        i=i+1; % Go on to the next time step.
    end
    % Since we're solving for the residence time at each stage, keep track
    % of the final time step for the stages before the product moves to the
    % next one. This can later be used to determine residence times.
    StTime(k) = i;
    k = k+1; % Go on the next stage.
end

% Print out the residence times for each stage
for k=2:Nst
    StTime(k) = StTime(k) - StTime(k-1);
end
StTime = StTime*dt;

% -------------------------------------------------------------------------
% --- Part C --------------------------------------------------------------
% -------------------------------------------------------------------------
k=1; % Set the stage 
Xd = Xinit/(1-Xinit); % Calculate the initial dry basis moisture content
% Determine the average diffusivity for the first stage
D = (diffusivity(Xst(1), Tst(k), phi) + diffusivity(Xd, Tst(k), phi))/2;
% Calculate the residence time at the first stage
StTimeA(1) = 4*L^2/(3.1415^2*D)*log(8*Xd/(3.1415^2*Xst(1)));
% Do the same for the rest of the stages
for k = 2:Nst
    D = (diffusivity(Xst(k-1), Tst(k), phi) + diffusivity(Xst(k-1), Tst(k), phi))/2;
    StTimeA(k) = 4*L^2/(pi^2*D)*log(8*Xst(k-1)/(pi^2*Xst(k)));
end

% Print out the residence times
fprintf('Stage residence times\n');
for k=1:Nst
    fprintf('Stage %d: Numerical: %g Analytical: %g\n', k, StTime(k), StTimeA(k));
end

for i=1:length(c(:,1))
    time(i) = dt*i;
    Ls(i) = sum(dx(i,:));
end

figure(2);
plot(time, Ls)
title('Thickness of Half of the Slab');
xlabel('Time [s]');
ylabel('Thickness [m]');

figure(3);
plot(time, c(:,1));
hold on;
plot(time, c(:,nx));
plot(time, Xavg);
hold off;
title('Concentration vs Time');
xlabel('Time [s]');
ylabel('Moisture content [kg/kg db]')