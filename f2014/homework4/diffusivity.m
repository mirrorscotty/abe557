function [ Deff ] = diffusivity( X, T, phi )
% Determine effective diffusivity using the model in Chapter 10 of The
%   Handbook of Food Engineering.
%       X: Moisture content [g/kg db]
%       T: Temperature [K]
%       phi: Porosity (as a decimal)

% Specify all the values required for the diffusivity equation.
K = 1032.558; % K value [dimensionless]
D_0L = 6.3910e-8; % Liquid D0 value[m^2/s]
D_0V = 2e-5; % Vapor D0 value[m^2/s]
Ea = 21760; % [J/mol]
R = 8.314; % Gas Constant [J/(mol K)]
% Calculate binding energy
Eb = binding_energy(X, T);

%Deff/D0
DD0 = exp(-Ea/(R*T)) * ( K*exp(-Eb/(R*T))/(1+K*exp(-Eb/(R*T))) );

% Calculate the D0 value at this porosity.
D0 = D_0L*(1-phi) + D_0V*phi;

% Determine effective diffusivity
Deff = DD0 * D0;

end

