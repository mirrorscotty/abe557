function [aw] = gab_aw(X, T)
% Calculate the water activity given moisture content and temperature.
%       X: Moisture content [kg/kg db]
%       T: Temperature [K]
%   The GAB Equation solved for water activity, where each parameter is a
%   function of temperature. The equation in quadratic form is:
%   0 = A*aw^2 + B*aw + C (source: Samaniego-Esguerra et al. 1991)
%       where:
%       A = K/Xm*(1/Cg-1)
%       B = 1/Xm*(1-2/Cg) - 1/X
%       C = 1/(Xm*Cg*K)

% Convert moisture content to a percent.
X = X*100;

% Parameters for soy calculated in part 1.
Xm0 = 0.6474;
dHxm = 606.0366;
Cg0 = 6.0651e-4;
dHcg = 3.2893e3;
K0 = 1.4097;
dHk = -127.5353;

% Determine the GAB temperature dependent constants.
Xm = Xm0*exp(dHxm/T);
Cg = Cg0*exp(dHcg/T);
K = K0*exp(dHk/T);

% Find values for A, B, and C.
A = K/Xm*(1/Cg-1);
B = 1/Xm*(1-2/Cg) - 1/X;
C = 1/(Xm*Cg*K);

% Determine water activity using the quadratic formula.
aw = (-B-sqrt(B^2-4*A*C))/(2*A);

end
