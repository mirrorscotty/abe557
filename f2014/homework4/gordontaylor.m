function [ Tg ] = gordontaylor( Xwb )
% Determine the glass transition temperature of soybeans using the
%   Gordon-Taylor equation.
%       Xwb - Moisture content [kg/kg wb]
%       Tg - Glass transition temperature [kg/m^3]

Tg1 = 445; % [K] Glass transition temperature of soild only
Tg2 = 136; % [K] Glass transition temperature of water only
k = 4.42;

% The Gordon-Taylor equation, where 1-Xwb = w1 and Xwb = w2.
Tg = ((1-Xwb)*Tg1+k*Xwb*Tg2) / ((1-Xwb)+k*Xwb);

end

