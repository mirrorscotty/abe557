function [rho] = rhosoy(Xdb, T)
% Calculate the density of soy as a function of temperature and
%   moisture content.
%       Xdb - Moisture Content [kg/kg db]
%       T - Temperature [K]
%
% Soy Composition (per 100g raw seeds):
%   Carbohydrates:  21.86g
%   Fat:            19.94g
%   Protein:        36.49g
%   Fiber:          9.3g
%   Ash:            3.83g
%   Water:          8.54g
% Source: http://http://en.wikipedia.org/wiki/Soybean

% Find the mass of water assuming 91.46g solids
Ms = 91.46; % Mass of solids per 100g
Mw = Xdb*Ms; % Mass of water

% Convert Temperature from K to C
T = T - 273.15;

% Calculate the mass fractions of each component
Mcar = 21.86/(Ms+Mw);
Mfat = 19.94/(Ms+Mw);
Mpro = 36.49/(Ms+Mw);
Mfib = 9.3/(Ms+Mw);
Mash = 3.83/(Ms+Mw);
Mwat = Mw/(Ms+Mw);

% Find the density of each component using the Choi-Okos equations
Ppro = 1.3299e3-5.1840e-1*T;
Pfat = 9.2559e2-4.1751e-1*T;
Pcar = 1.5991e3-3.1046e-1*T;
Pfib = 1.3115e3-3.6589e-1*T;
Pash = 2.4238e3-2.8063e-1*T;
Pwat = 9.9718e2+3.1439e-3*T-3.7574e-3*T^2;

% Combine all of the densities and mass fractions to find the overall
% density for the product.
rho = 1/(Mcar/Pcar+Mfat/Pfat+Mpro/Ppro+Mfib/Pfib+Mash/Pash+Mwat/Pwat);

end
