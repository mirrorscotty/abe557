% Clear out any variables that might be in Matlab's current workspace so we
% can start from a clean slate.
clear;

%----------------------- Given Information --------------------------------
% Slab thickness
L = 0.01; % m
% Initial concentration
c0 = 1; % kg-mol/m^3
% Concentration along the boundary
cb = 0;
% Diffusivity
Dab = 1e-10; % m^2/s
% Final time
tf = 12e4; % s
% Delta X value to use when solving
dx = 0.0005; % m
% Supplied value of M (for use in finite difference equations)
M = 2;

%--- Set-up ---------------------------------------------------------------
% Calculate the delta t value we'll be using for the solution. This is
% determined from the equation for M.
% M = (dx)^2/(Dab*dt)
dt = dx^2/(Dab*M);

% Determine the number of time steps required to obtain the profile at the
% final time. Because matricies in Matlab start indexing with 1 instead of
% 0, we need to add one to this value.
nt = tf/dt+1;

% Calculate the number of nodes to use. For the same reason as above, 1 has
% been added to this value.
nx = L/dx+1;

%--- Impose Initial Condition ---------------------------------------------
% Set the initial condition. Here, the variable c will hold all of the
% values we calculate. The i index will represent the current time step,
% and the j index will be the current point along the x-axis.
for j = 1:nx
    c(1,j) = c0;
end

% For increased numerical stability, set the value of the node on the
% boundary at the initial time step to half way between the initial value
% and the value it will be taking later on.
c(1,1) = (c0+cb)/2;

%--- Finite Difference Calculations ---------------------------------------
% Calculate the concentration profiles at each time step.
for i = 2:nt
    % Impose the boundary condition at x=0
    c(i,1) = cb;
    % Calculate the concentration at each point along the interior of the
    % slab using the finite difference method.
    for j = 2:nx-1
       c(i,j) = 1/M * (c(i-1,j+1) + (M-2)*c(i-1,j) + c(i-1,j-1));
    end
    % At x=L, impose an insulated boundary condition.
    c(i,nx) = 1/M * ((M-2)*c(i-1,nx) + 2*c(i-1,nx-1));
end

%--- Output ---------------------------------------------------------------
% Print out the concentration profile at t=tf.
c(nt,:)
